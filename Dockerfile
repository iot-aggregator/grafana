FROM grafana/grafana

COPY ./config/provisioning/datasources /etc/grafana/provisioning/datasources

COPY ./config/provisioning/dashboards /etc/grafana/provisioning/dashboards

COPY ./config/dashboards /var/lib/grafana/dashboards

COPY ./config/config.ini /etc/grafana/config.ini